import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { Message } from '@/definitions/Message'

export const useMessageStore = defineStore(
    'message',
    () => {
        const lastMessage = ref<Message>()

        const hasLastMessage = computed(() => !!lastMessage.value)

        return { lastMessage, hasLastMessage }
    },
    { persist: true }
)
