import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { Gadget } from '@/definitions/Gadget.ts'

export const useGadgetStore = defineStore(
    'gadget',
    () => {
        const gadgets = ref<Array<Gadget>>([])

        const hasGadgets = computed(() => gadgets.value.length > 0)

        function addGadget(gadget: Gadget) {
            gadgets.value.push(gadget)
        }

        function getById(id: string) {
            return gadgets.value.find((gadget) => gadget.id === id)
        }

        function getByLinkedId(id: number) {
            return gadgets.value.find((gadget) => gadget.linkedId === id)
        }

        function updateById(id: string, gadget: Gadget) {
            const foundIndex = getIndexById(id)
            gadgets.value[foundIndex] = gadget
        }

        function deleteById(id: string) {
            gadgets.value.splice(getIndexById(id), 1)
        }

        function getIndexById(id: string) {
            return gadgets.value.findIndex((searchGadget) => searchGadget.id == id)
        }

        return { gadgets, hasGadgets, addGadget, getById, updateById, deleteById, getByLinkedId }
    },
    { persist: true }
)
