import { defineStore } from 'pinia'
import { computed, ref } from 'vue'

export const useUserStore = defineStore(
    'user',
    () => {
        const name = ref<string>()

        const token = ref<string>()

        const id = ref<number>()

        const forcedLogout = ref<boolean>(false)

        const isLoggedIn = computed(() => !!token.value)

        function login(pName: string, pToken: string, pId: number) {
            name.value = pName
            token.value = pToken
            id.value = pId
            forcedLogout.value = false
        }

        function forceLogout() {
            forcedLogout.value = true
            logout()
        }

        function logout() {
            name.value = undefined
            token.value = undefined
        }

        return { name, token, id, isLoggedIn, forcedLogout, login, logout, forceLogout }
    },
    { persist: true }
)
