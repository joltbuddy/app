import { cleanupOutdatedCaches, precacheAndRoute } from 'workbox-precaching'

declare let self: ServiceWorkerGlobalScope

cleanupOutdatedCaches()

precacheAndRoute(self.__WB_MANIFEST)

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
self.__WB_DISABLE_DEV_LOGS = true

self.addEventListener('message', (event) => {
    if (event.data && event.data.type === 'SKIP_WAITING') self.skipWaiting()
})

self.addEventListener('push', function (event) {
    if (!event.data) {
        console.log('This push event has no data.')
        return
    }
    if (!self.registration) {
        console.log('Service worker does not control the page')
        return
    }
    if (!self.registration || !self.registration.pushManager) {
        console.log('Push is not supported')
        return
    }

    const eventText = event.data.text()
    // Specify default options
    let options = {}
    let title = ''

    // Support both plain text notification and json
    if (eventText.substring(0, 1) === '{') {
        const eventData = JSON.parse(eventText)
        title = eventData.title

        // Set specific options
        // @link https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification#parameters
        if (eventData.options) {
            options = Object.assign(options, eventData.options)
        }

        // Check expiration if specified
        if (eventData.expires && Date.now() > eventData.expires) {
            console.log('Push notification has expired')
            return
        }
    } else {
        title = eventText
    }

    // Warning: this can fail silently if notifications are disabled at system level
    // The promise itself resolve to undefined and is not helpful to see if it has been displayed properly
    const promiseChain = self.clients
        .matchAll({
            type: 'window',
            includeUncontrolled: true,
        })
        .then((windowClients) => {
            windowClients.forEach((windowClient) => {
                windowClient.postMessage({
                    message: eventText,
                    time: new Date().toString(),
                })
            })
            // Client isn't focused, we need to show a notification.
            return self.registration.showNotification(title, options)
        })

    // With this, the browser will keep the service worker running until the promise you passed in has settled.
    event.waitUntil(promiseChain)
})
