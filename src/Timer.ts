export class Timer {
    startTime: DOMHighResTimeStamp

    constructor() {
        this.startTime = performance.now()
    }

    getMsSinceStart(): number {
        const currentTime = performance.now()

        return Math.floor(currentTime - this.startTime) //in ms
    }
}
