import App from './App.vue'
import './style.css'

import { createApp } from 'vue'

const app = createApp(App)

// Pinia store
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)
app.use(pinia)

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import '@mdi/font/css/materialdesignicons.css'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import colors from 'vuetify/lib/util/colors'

// EventTarget Prototype modifications
import { addPauseEventListenersToEventTargetPrototype } from '@/EventType.extensions.ts'

const vuetify = createVuetify({
    components,
    directives,
    theme: {
        themes: {
            light: {
                dark: false,
                colors: {
                    primary: colors.deepOrange.darken4,
                    secondary: colors.deepPurple.lighten2,
                },
            },
        },
    },
})
app.use(vuetify)

app.mount('#app')

addPauseEventListenersToEventTargetPrototype()
