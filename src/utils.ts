import { useUserStore } from '@stores/UserStore.ts'

export function requestWithToken(url: string | URL, method: 'POST' | 'GET' = 'GET', headers: HeadersInit | undefined = undefined, body: BodyInit | undefined = undefined) {
    const userStore = useUserStore()

    return fetch(url, {
        method,
        headers: {
            Authorization: 'Bearer ' + userStore.token,
            ...headers,
        },
        body,
    }).then((response) => {
        if (response.status === 401) {
            userStore.forceLogout()
            return response
        }

        return response
    })
}
