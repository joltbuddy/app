import { useMessageStore } from '@stores/MessageStore.ts'

import { BluetoothRemoteGATTCharacteristicWithEventTarget } from '@/EventType.extensions.ts'
import { requestWithToken } from '@/utils.ts'

export class MessageManager {
    messageStore

    constructor() {
        this.messageStore = useMessageStore()
    }

    async onDeviceValueNotification(event: Event, targetId: number) {
        const characteristic = event.target as BluetoothRemoteGATTCharacteristicWithEventTarget
        const rawValue = characteristic.value
        let finished = false

        characteristic.pauseEventListeners('characteristicvaluechanged')

        let valueString = new TextDecoder().decode(rawValue)

        let newMessage = valueString

        while (!finished) {
            const rawValue = await characteristic.readValue()
            valueString = new TextDecoder().decode(rawValue)

            newMessage += valueString

            if (valueString.includes('END')) {
                finished = true
                break
            }
        }

        console.log('Finished!')

        console.log('Final raw message: ', newMessage)

        newMessage = newMessage.split('_')[0]

        console.log('Final message: ', newMessage)

        characteristic.resumeEventListeners('characteristicvaluechanged')

        this.sendNewMessage(newMessage, targetId)
    }

    sendNewMessage(value: string, targetId: number) {
        console.log('message to ', targetId)

        const apiSendMessageUrl = import.meta.env.VITE_API_URL + '/push/send'
        requestWithToken(
            apiSendMessageUrl,
            'POST',
            {
                'Content-Type': 'application/json',
            },
            JSON.stringify({
                to: targetId,
                message: value,
            })
        ).then((response) => {
            if (response?.status !== 200) {
                console.error(response)
            }
        })
    }
}
