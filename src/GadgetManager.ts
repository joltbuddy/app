import { useGadgetStore } from '@stores/GadgetStore.ts'
import { Gadget } from '@/definitions/Gadget.ts'
import { reactive } from 'vue'
import { MessageManager } from '@/MessageManager.ts'

export function bluetoothSupported() {
    return 'bluetooth' in navigator
}

export async function connectToGadget(valueChangeCallback: (e: Event, id: number) => void, messageContext: MessageManager) {
    if (!bluetoothSupported()) return

    const device = await navigator.bluetooth.requestDevice({
        filters: [
            {
                services: ['user_data'],
            },
        ],
    })

    if (!device) return

    const gadget = reactive(new Gadget(device))

    await gadget.connect()

    await gadget.registerValueCallback(valueChangeCallback, messageContext)

    const gadgetStore = useGadgetStore()

    const existingGadget = gadgetStore.getById(gadget.id)

    if (existingGadget) {
        gadget.setLinked(existingGadget.linkedId, existingGadget.linkedName)
        gadgetStore.updateById(gadget.id, gadget)
    } else {
        gadgetStore.addGadget(gadget)
    }

    return gadget
}

export async function replayMessage(gadget: Gadget, message: string) {
    const characteristic = gadget.notificationCharacteristic

    if (!characteristic) return false

    await sendMessage(message, characteristic)

    return true
}

async function sendMessage(valueString: string, characteristic: BluetoothRemoteGATTCharacteristic) {
    console.log('sending', valueString)

    const maxPacketSize = 244

    const splitValues = valueString.match(new RegExp('.{1,' + maxPacketSize + '}', 'g'))

    console.log(splitValues)

    if (!splitValues) {
        console.error('could not split message', valueString)
        return
    }

    for (const value of splitValues) {
        const encodedValue = new TextEncoder().encode(value)
        await characteristic.writeValueWithResponse(encodedValue)
    }

    const encodedEnd = new TextEncoder().encode('END')
    await characteristic.writeValueWithResponse(encodedEnd)
}
