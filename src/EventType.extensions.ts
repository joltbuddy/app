/* eslint-disable @typescript-eslint/ban-ts-comment */

// @ts-ignore
export interface BluetoothRemoteGATTCharacteristicWithEventTarget extends EventTargetWithPauseAndResumeUtils, BluetoothRemoteGATTCharacteristic {}

interface EventTargetWithPauseAndResumeUtils extends EventTarget {
    pauseEventListeners(targetType: string): void
    resumeEventListeners(targetType: string): void
}

export function addPauseEventListenersToEventTargetPrototype() {
    const originalAddEventListener = EventTarget.prototype.addEventListener

    const symbolHidden = Symbol('hidden')

    function hidden(instance: EventTarget) {
        // @ts-ignore
        if (instance[symbolHidden] === undefined) {
            const area = {}
            // @ts-ignore
            instance[symbolHidden] = area
            return area
        }

        // @ts-ignore
        return instance[symbolHidden]
    }

    function listenersFrom(instance: EventTarget) {
        const area = hidden(instance)
        if (!area.listeners) {
            area.listeners = []
        }
        return area.listeners
    }

    function pausedFrom(instance: EventTarget) {
        const area = hidden(instance)
        if (!area.paused) {
            area.paused = []
        }
        return area.paused
    }

    EventTarget.prototype.addEventListener = function (type, listener) {
        const listeners = listenersFrom(this)

        listeners.push({ type, listener })

        originalAddEventListener.apply(this, [type, listener])
    }

    // @ts-ignore
    EventTarget.prototype.pauseEventListeners = function (targetType: string) {
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const self = this

        const listeners = listenersFrom(this)
        const paused = pausedFrom(this)

        listeners.forEach((item: any) => {
            const type = item.type
            const listener = item.listener

            if (type == targetType) {
                // @ts-ignore
                self.removeEventListener(type, listener)
                paused.push({
                    type,
                    listener,
                })
            }
        })
    }

    // @ts-ignore
    EventTarget.prototype.resumeEventListeners = function (targetType: string) {
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const self = this

        const paused = pausedFrom(this)

        paused.forEach((item: any) => {
            const type = item.type
            const listener = item.listener

            if (type == targetType) {
                self.addEventListener(type, listener)
            }
        })
    }
}
