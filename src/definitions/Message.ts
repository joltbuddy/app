export class Message {
    targetId: number
    value: string
    timestamp: string | undefined

    constructor(value: string, targetId: number) {
        this.targetId = targetId
        this.value = value
        const now = new Date()
        this.timestamp = `${now.toLocaleDateString()} ${now.toLocaleTimeString()}`
    }
}
