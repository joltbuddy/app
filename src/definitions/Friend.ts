export class Friend {
    id: number
    username: string

    constructor(value: string, id: number) {
        this.id = id
        this.username = value
    }
}
