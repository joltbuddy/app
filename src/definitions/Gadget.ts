import { MessageManager } from '@/MessageManager.ts'

export class Gadget {
    readonly device: BluetoothDevice
    server: BluetoothRemoteGATTServer | undefined
    notificationCharacteristic: BluetoothRemoteGATTCharacteristic | undefined
    readonly name: string | undefined
    readonly id: string
    linkedId = 0
    linkedName = 'Noone :('
    connected: boolean

    constructor(device: BluetoothDevice) {
        this.device = device
        this.name = device.name
        this.id = device.id
        this.connected = false
    }

    static fromRawGadget(pGadget: Gadget) {
        const gadget: Gadget = new Gadget(pGadget.device)
        return Object.assign(gadget, pGadget)
    }

    setLinked(linkedId: number, linkedName: string) {
        this.linkedId = linkedId
        this.linkedName = linkedName
    }

    async connect() {
        this.device.addEventListener('gattserverdisconnected', this.onDisconnect.bind(this))
        this.server = (await this.device.gatt?.connect()) as BluetoothRemoteGATTServer

        if (this.server) {
            this.connected = true
        }
    }

    disconnect() {
        this.server?.disconnect()
    }

    forget() {
        this.device?.forget()
    }

    getConnected() {
        return this.connected
    }

    async registerValueCallback(callback: (e: Event, id: number) => void, messageContext: MessageManager) {
        const service = await this.server?.getPrimaryService('user_data')

        this.notificationCharacteristic = await service?.getCharacteristic('new_alert')

        const dataCharacteristic = await service?.getCharacteristic('scan_refresh')

        dataCharacteristic?.addEventListener('characteristicvaluechanged', (e) => callback.apply(messageContext, [e, this.linkedId]))

        await dataCharacteristic?.startNotifications()
    }

    onDisconnect() {
        console.log('Device ' + this.name + ' with id ' + this.id + ' was disconnected')
        this.connected = false
    }
}
