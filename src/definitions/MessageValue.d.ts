interface MessageValue {
    color: number
    vibrationIntensity: number
    startTime: DOMHighResTimeStamp
    endTime: DOMHighResTimeStamp
}
