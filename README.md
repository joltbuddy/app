# JoltBuddy App

This is the home of the supporting App for JoltBuddy gadgets.
For portability and accessibility reasons, this app is a PWA utilizing web technologies like ServiceWorker, WebPush and WebBluetooth.

It is written in Vue.js 3 with TypeScript and Vite.

## Running the app locally

-   Install dependencies using `npm i`
-   Run the app using `npm run dev`
